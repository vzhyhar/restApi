package com.mbicycle.test.api.service;

import com.mbicycle.test.api.entity.Seller;

import java.util.List;
import java.util.Optional;

public interface SellerService {
    List<Seller> getAllSellers();
    void addSeller(Seller seller);
    void deleteSeller(int id);
    Seller updateSeller(Seller seller);
    Optional<Seller> getSellerById(int id);
}
