package com.mbicycle.test.api.service;

import com.mbicycle.test.api.entity.Deal;
import com.mbicycle.test.api.repository.DealRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DealServiceImpl implements DealService {
    @Autowired
    private DealRepository dealRepository;

    @Override
    public List<Deal> getAllSellerDeals(int sellerId) {
        return dealRepository.findBySellerSellerId(sellerId);
    }

    @Override
    public Deal getDealById(int dealId) {
        return dealRepository.findById(dealId).get();
    }

    @Override
    public void addDeal(Deal deal) {
        dealRepository.save(deal);
        System.out.println("saved");
    }

    @Override
    public void removeDealById(int dealId) {
        dealRepository.deleteById(dealId);
    }

    @Override
    public Deal updateDeal(Deal deal) {
        return dealRepository.save(deal);
    }
}
