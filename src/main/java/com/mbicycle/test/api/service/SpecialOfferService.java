package com.mbicycle.test.api.service;

import com.mbicycle.test.api.entity.SpecialOffer;

import java.util.List;

public interface SpecialOfferService {
    List<SpecialOffer> getAllOffers();
    SpecialOffer getOfferById(int dealId);
    void addOffer(SpecialOffer offer);
    void removeOfferById(int dealId);
    SpecialOffer updateOffer(SpecialOffer offer);
}
