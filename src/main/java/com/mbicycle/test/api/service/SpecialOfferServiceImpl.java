package com.mbicycle.test.api.service;

import com.mbicycle.test.api.entity.SpecialOffer;
import com.mbicycle.test.api.repository.SpecialOfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class SpecialOfferServiceImpl implements SpecialOfferService {
    @Autowired
    private SpecialOfferRepository repository;

    @Override
    public List<SpecialOffer> getAllOffers() {
        return repository.findAll();
    }

    @Override
    public SpecialOffer getOfferById(int id) {
        return repository.findById(id).get();
    }

    @Override
    public void addOffer(SpecialOffer offer) {
        repository.save(offer);
    }

    @Override
    public void removeOfferById(int dealId) {
        repository.deleteById(dealId);
    }

    @Override
    public SpecialOffer updateOffer(SpecialOffer offer) {
        return repository.save(offer);
    }
}
