package com.mbicycle.test.api.service;

import com.mbicycle.test.api.entity.Deal;

import java.util.List;

public interface DealService {
    List<Deal> getAllSellerDeals(int sellerId);
    Deal getDealById(int dealId);
    void addDeal(Deal deal);
    void removeDealById(int dealId);
    Deal updateDeal(Deal deal);
}
