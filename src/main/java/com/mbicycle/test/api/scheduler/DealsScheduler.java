package com.mbicycle.test.api.scheduler;

import com.mbicycle.test.api.entity.Deal;
import com.mbicycle.test.api.entity.SpecialOffer;
import com.mbicycle.test.api.repository.DealRepository;
import com.mbicycle.test.api.repository.SpecialOfferRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@Component
@Transactional
public class DealsScheduler {
    private List<Integer> added = new CopyOnWriteArrayList<>();

    @Autowired
    private SpecialOfferRepository offerRepository;

    @Autowired
    private DealRepository dealRepository;

    @Scheduled(fixedRate = 10_000)
    public void addDeals() {
        List<SpecialOffer> offerList = offerRepository.findAll();
        offerList.forEach(specialOffer -> {
            LocalDate date = LocalDate.now();
            Time current = Time.valueOf(LocalTime.now());
            if (specialOffer.getDayOfWeek().equals(date.getDayOfWeek()) &&
                    specialOffer.getStartTime().before(current) &&
                    specialOffer.getEndTime().after(current) &&
                    !specialOffer.isActive()) {
                specialOffer.setActive(true);
                Deal deal = new Deal();
                deal.setSeller(specialOffer.getSeller());
                deal.setBanner(specialOffer.getBanner());
                deal.setHeader(specialOffer.getHeader());
                deal.setPrice(specialOffer.getPrice());
                deal.setText(specialOffer.getText());
                dealRepository.save(deal);
                added.add(deal.getDealId());
                System.out.println("added: " + added);
            }
        });
    }

    @Scheduled(fixedRate = 30_000)
    public void removeDeals() {
        List<SpecialOffer> offers = offerRepository.findAll();
        System.out.println("remove:" + offers);
        offers.forEach(specialOffer -> {
            Time current = Time.valueOf(LocalTime.now());
            List<Deal> deals = dealRepository.findAll();
            deals.forEach(deal -> {
                if (specialOffer.isActive() &&
                        specialOffer.getEndTime().before(current) &&
                        added.contains(deal.getDealId())) {
                    specialOffer.setActive(false);
                    System.out.println(specialOffer.isActive());
                    dealRepository.deleteById(deal.getDealId());
                    added.remove(new Integer(deal.getDealId()));
                    System.out.println("remove:" + offers);
                    System.out.println("active?: " + specialOffer.isActive());
                }
            });
        });
    }
}
