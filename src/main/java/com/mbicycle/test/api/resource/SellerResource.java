package com.mbicycle.test.api.resource;

import com.mbicycle.test.api.entity.ApiError;
import com.mbicycle.test.api.entity.Seller;
import com.mbicycle.test.api.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/sellers")
public class SellerResource {
    @Autowired
    @Qualifier("sellerServiceImpl")
    private SellerService sellerService;

    @GetMapping()
    public ResponseEntity<List<Seller>> getAllSellers() {
        List<Seller> sellers = sellerService.getAllSellers();
        if (sellers == null || sellers.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return ResponseEntity.ok(sellers);
    }

    @GetMapping("{sellerId}")
    public ResponseEntity<?> getSellerById(@PathVariable("sellerId") int sellerId) {
        Optional<Seller> seller = sellerService.getSellerById(sellerId);
        if (!seller.isPresent()) {
            ApiError error = new ApiError("Seller with id " + sellerId + " does not exist", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(seller.get());
    }

    @PostMapping()
    public ResponseEntity<?> addSeller(@RequestBody Seller seller) {
        sellerService.addSeller(seller);
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().path("/{id}")
                .buildAndExpand(seller.getSellerId()).toUri());
        return new ResponseEntity<>(seller, headers, HttpStatus.CREATED);
    }

    @DeleteMapping("{sellerId}")
    public void deleteSeller(@PathVariable("sellerId") int sellerId) {
        sellerService.deleteSeller(sellerId);
    }

    @PutMapping("{sellerId}")
    public ResponseEntity<?> updateSeller(@PathVariable("sellerId") int sellerId,
                                               @RequestBody Seller seller) {
        seller.setSellerId(sellerId);
        Optional<Seller> optSeller = sellerService.getSellerById(sellerId);
        if (!optSeller.isPresent()) {
            ApiError error = new ApiError("Seller with id " + sellerId + " does not exist", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
        }
        sellerService.updateSeller(seller);
        return new ResponseEntity<>(seller, HttpStatus.CREATED);
    }

}
