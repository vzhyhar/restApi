package com.mbicycle.test.api.resource;

import com.mbicycle.test.api.entity.ApiError;
import com.mbicycle.test.api.entity.Deal;
import com.mbicycle.test.api.entity.Seller;
import com.mbicycle.test.api.service.DealService;
import com.mbicycle.test.api.service.SellerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/sellers/{sellerId}/deals")
public class DealResource {
    @Autowired
    @Qualifier("dealServiceImpl")
    private DealService dealService;

    @Autowired
    @Qualifier("sellerServiceImpl")
    private SellerService sellerService;

    @GetMapping()
    public ResponseEntity<List<Deal>> getAllDealsBySellerId(@PathVariable("sellerId") int sellerId) {
        List<Deal> deals = dealService.getAllSellerDeals(sellerId);
        if (deals.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        HttpHeaders headers = new HttpHeaders();
        Link link = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(SellerResource.class)
                .getSellerById(sellerId))
                .withRel("seller");
        deals.forEach(deal -> deal.add(link));
        return ResponseEntity.ok(deals);
    }

    @GetMapping("{dealId}")
    public ResponseEntity<Deal> getDealById(@PathVariable("sellerId") int sellerId,
                                            @PathVariable("dealId") int dealId) {
        Deal deal = dealService.getDealById(dealId);
        if (deal == null) {
            return ResponseEntity.notFound().build();
        }
        Link link = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(SellerResource.class)
                .getSellerById(sellerId))
                .withRel("seller");
        deal.add(link);
        return ResponseEntity.ok(deal);
    }

    @PostMapping()
    public ResponseEntity<?> addDeal(@PathVariable("sellerId") int sellerId,
                                  @RequestBody Deal deal) {
        Optional<Seller> seller = sellerService.getSellerById(sellerId);
        System.out.println(seller.getClass());
        if (!seller.isPresent()) {
            ApiError error = new ApiError("Seller with id " + sellerId + " does not exist", HttpStatus.NOT_FOUND);
            return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
        }
        deal.setSeller(new Seller(sellerId));
        dealService.addDeal(deal);
        return new ResponseEntity(deal, HttpStatus.CREATED);
    }

    @DeleteMapping("{dealId}")
    public void deleteDeal(@PathVariable("dealId") int dealId) {
        dealService.removeDealById(dealId);
    }

    @PutMapping("{dealId}")
    public ResponseEntity<Deal> updateDeal(@PathVariable("sellerId") int sellerId,
                           @PathVariable("dealId") int dealId,
                           @RequestBody Deal deal) {
        deal.setSeller(new Seller(sellerId));
        deal.setDealId(dealId);
        dealService.updateDeal(deal);
        return new ResponseEntity<>(deal, HttpStatus.CREATED);
    }
}
