package com.mbicycle.test.api.resource;

import com.mbicycle.test.api.entity.Seller;
import com.mbicycle.test.api.entity.SpecialOffer;
import com.mbicycle.test.api.service.SpecialOfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sellers/{sellerId}/offers")
public class SpecialOfferResource {
    @Autowired
    @Qualifier("specialOfferServiceImpl")
    private SpecialOfferService specialOfferService;

    @GetMapping
    public ResponseEntity<List<SpecialOffer>> getAllSpecialOffers() {
        List<SpecialOffer> offers = specialOfferService.getAllOffers();
        System.out.println(offers.size());
        System.out.println(offers.isEmpty());
        if (offers.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        offers.forEach(offer -> {
            Seller seller = offer.getSeller();
            Link link = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(SellerResource.class)
                    .getSellerById(seller.getSellerId()))
                    .withRel("seller");
            offer.add(link);
        });

        return ResponseEntity.ok(offers);
    }

    @GetMapping("{offerId}")
    public ResponseEntity<SpecialOffer> getOfferById(@PathVariable("sellerId") int sellerId,
                                                     @PathVariable("offerId") int offerId) {
        SpecialOffer offer = specialOfferService.getOfferById(offerId);
        if (offer == null) {
            return ResponseEntity.notFound().build();
        }
        Link link = ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(SellerResource.class)
                .getSellerById(sellerId))
                .withRel("seller");
        offer.add(link);
        return ResponseEntity.ok(offer);
    }

    @PostMapping
    public ResponseEntity<SpecialOffer> addSpecialOffer(@PathVariable("sellerId") int sellerId,
                                                        @RequestBody SpecialOffer offer) {
        Seller seller = new Seller(sellerId);
        offer.setSeller(seller);
        specialOfferService.addOffer(offer);
        return new ResponseEntity<>(offer, HttpStatus.CREATED);
    }

    @DeleteMapping("{offerId}")
    public void deleteSpecialOffer(@PathVariable("offerId") int offerId) {
        specialOfferService.removeOfferById(offerId);
    }

    @PutMapping("{offerId}")
    public ResponseEntity<SpecialOffer> updateSpecialOffer(@PathVariable("sellerId") int sellerId,
                                                           @PathVariable("offerId") int offerId,
                                                           @RequestBody SpecialOffer offer) {
        offer.setSeller(new Seller(sellerId));
        offer.setDealId(offerId);
        specialOfferService.updateOffer(offer);
        return new ResponseEntity<>(offer, HttpStatus.CREATED);
    }
}
