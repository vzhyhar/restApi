package com.mbicycle.test.api.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.*;

@Entity
@Table(name = "DEAL")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Deal extends ResourceSupport {
    @Id
    @GeneratedValue
    private int dealId;
    private String header;
    private String text;
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="seller_id")
    private Seller seller;
    private int price;
    @Lob
    private byte[] banner;
}
