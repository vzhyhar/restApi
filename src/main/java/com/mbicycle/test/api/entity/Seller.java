package com.mbicycle.test.api.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "SELLER")
@Data
@NoArgsConstructor
@RequiredArgsConstructor
public class Seller {
    @Id
    @GeneratedValue
    @NonNull
    private int sellerId;
    private String name;
    @OneToMany(cascade = CascadeType.REMOVE, mappedBy = "seller")
    private List<Deal> deals;

}
