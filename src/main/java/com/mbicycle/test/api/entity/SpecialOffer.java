package com.mbicycle.test.api.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.util.Date;

@Entity
@Data
public class SpecialOffer extends Deal {
    @Enumerated(EnumType.ORDINAL)
    private DayOfWeek dayOfWeek;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "GMT+3")
    @Temporal(TemporalType.TIME)
    private Date startTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss", timezone = "GMT+3")
    @Temporal(TemporalType.TIME)
    private Date endTime;
    @JsonIgnore
    private boolean active;
}
