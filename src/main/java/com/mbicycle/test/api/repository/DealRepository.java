package com.mbicycle.test.api.repository;

import com.mbicycle.test.api.entity.Deal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DealRepository extends JpaRepository<Deal, Integer> {
    List<Deal> findBySellerSellerId(int id);
}
